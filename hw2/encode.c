#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "encode.h"

int main(int argc, char *argv[]) 
{
    FILE *input_file, *output_file;
    const struct coding_page encoding_variants[3] = { cp1251, koi8r, iso8859 };
    const char **encoding = NULL;
    struct stat file_stat;

    if (argc != 4) {
        printf("Usage: encode <INPUT_FILE> <OUTPUT_FILE> {cp1251|koi8r|iso8859}\n");
        return EXIT_FAILURE;
    }

    for (unsigned int i=0; i < sizeof(encoding_variants)/sizeof(encoding_variants[0]); i++) {
        if (!strcmp(encoding_variants[i].name, argv[3])) {
            encoding = encoding_variants[i].table;
            break;
        }
    }

    if (!encoding) {
        printf("Unknown encoding.\n");
        return EXIT_FAILURE;
    }

    input_file = fopen(argv[1], "r");
    if (!input_file) {
        printf("Failed open %s: %s\n", argv[1], strerror(errno));
        return EXIT_FAILURE;
    }

    output_file = fopen(argv[2], "w");
    if (!output_file) {
        printf("Failed create %s: %s\n", argv[2], strerror(errno));
        return EXIT_FAILURE;
    }

    if (stat(argv[1], &file_stat) == -1) {
        printf("Cannot stat file %s: %s\n", argv[1], strerror(errno));
        return EXIT_FAILURE;
    }

    for (long i=0; i < file_stat.st_size; i++) {
        unsigned char input_char;
        fread(&input_char, sizeof(char), 1, input_file);
        if (input_char < 0x80) {
            fwrite(&input_char, sizeof(char), 1, output_file);
        } else {
            unsigned char index = input_char - 0x80;
            fwrite(encoding[index], sizeof(char), strlen(encoding[index]), output_file);
        }
    }
    printf("Converting complete.\n");

    fclose(input_file);
    fclose(output_file);
    return EXIT_SUCCESS;
}
