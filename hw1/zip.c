#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "zip.h"

int main(int argc, char *argv[]) 
{
    char ecdr_signature[4] = {0x50, 0x4b, 0x05, 0x06}; // End of central directory record signature
    FILE *test_file;
    long chunk_size = 8196;
    long start_cdfh = 0;
    struct stat file_stat;
    struct end_central_directory_record ecdr;
    struct central_directory_file_header cdfh;

    if(argc != 2) {
        printf("Wrong numbers of arguments, only one is required and allowed.");
        return EXIT_FAILURE;
    }
    
    test_file = fopen(argv[1], "r");
    if (!test_file) {
        printf("Failed open %s: %s\n", argv[1], strerror(errno));
        return EXIT_FAILURE;
    }

    if (stat(argv[1], &file_stat) == -1) {
        printf("Cannot stat file %s: %s\n", argv[1], strerror(errno));
        return EXIT_FAILURE;
    }

    if(file_stat.st_size < (long)(chunk_size + sizeof(ecdr.signature))) {
        chunk_size = file_stat.st_size;
    }
    char buffer[chunk_size+sizeof(ecdr_signature)];

    for(long i=0; i < file_stat.st_size; i+=chunk_size) {
        int work_bytes = fread(buffer+sizeof(ecdr_signature), sizeof(char), chunk_size, test_file);
        for(int j=0; j < chunk_size; j++) {
            if(memcmp(ecdr_signature, buffer+j, sizeof(ecdr_signature)) == 0) {
                memcpy(&ecdr, buffer + j, sizeof(ecdr));
                start_cdfh = i + j - ecdr.sizeOfCentralDirectory - sizeof(ecdr_signature);
                printf("Found \"End of central directory record\".\n");
                break;
            }
        }
        memcpy(buffer, buffer+work_bytes, sizeof(ecdr_signature));
    }

    if (!start_cdfh) {
        printf("ZIP not found.\n");
        fclose(test_file);
        return EXIT_SUCCESS;
    }

    printf("Found %d central directory records:\n", ecdr.totalCentralDirectoryRecord);
    char all_cdr[ecdr.sizeOfCentralDirectory];
    unsigned long offset = 0;
    fseek(test_file, start_cdfh, SEEK_SET);
    fread(all_cdr, sizeof(char), sizeof(all_cdr), test_file);
    for(int i=0; i < ecdr.totalCentralDirectoryRecord; i++) {
        memcpy(&cdfh, all_cdr+offset, sizeof(cdfh));
        printf("%.*s\n", cdfh.filenameLength,  all_cdr+offset+sizeof(cdfh));
        offset += sizeof(cdfh) + cdfh.filenameLength + cdfh.extraFieldLength + cdfh.fileCommentLength;
    }

    fclose(test_file);
    return EXIT_SUCCESS;
}
